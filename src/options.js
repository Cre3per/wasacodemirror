class Options
{
  async _get(key)
  {
    if (chrome)
    {
      return new Promise((resolve) =>
      {
        chrome.storage.local.get(key, values => resolve(values[key]));
      });
    }
    else
    {
      return (await browser.storage.local.get(key))[key];
    }
  }

  async _set(key, value)
  { 
    let request = {};
    request[key] = value;

    if (chrome)
    {
      return new Promise((resolve) =>
      {
        chrome.storage.local.set(request, resolve);
      });
    }
    else
    {
      return browser.storage.local.set(request);
    }
  }

  constructor()
  {
    (async () => {
      let vimrc = await this._get('.vimrc');
      let codeMirrorOverride = await this._get('code-mirror-override');

      let textVimrc = document.getElementById('vimrc');
      let textOverride = document.getElementById('code-mirror-override');
      let button = document.getElementById('save');

      if (vimrc)
      {
        textVimrc.value = vimrc;
      }

      if (codeMirrorOverride)
      {
        textOverride.value = codeMirrorOverride;
      }
      
      button.onclick = () => {
        this._set('.vimrc', textVimrc.value);
        this._set('code-mirror-override', textOverride.value);
      }
    })().catch(console.error);
  }
}

new Options();