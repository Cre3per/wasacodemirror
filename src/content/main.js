class Main
{
  /**
   * 
   */
  async _get(key)
  {
    if (chrome)
    {
      return new Promise((resolve) =>
      {
        chrome.storage.local.get(key, values => resolve(values[key]));
      });
    }
    else
    {
      return (await browser.storage.local.get(key))[key];
    }
  }

  /**
   * Registers custom code mirror vim functions and variables.
   */
  _setupCodeMirror()
  {
    CodeMirror.Vim.defineEx('quit', 'q', (codeMirror, args) =>
    {
      codeMirror.toTextArea();
      codeMirror.getTextArea().focus();
    });

    CodeMirror.Vim.defineEx('w', undefined, (codeMirror, args) =>
    {
      codeMirror.save();
    });

    CodeMirror.Vim.defineEx('wq', undefined, (codeMirror, args) =>
    {
      codeMirror.toTextArea();
      codeMirror.getTextArea().focus();
    });

    CodeMirror.Vim.defineOption('number', undefined, 'boolean', undefined, (value, codeMirror) =>
    {
      if (codeMirror)
      {
        if (value === undefined)
        {
          return codeMirror.getOption('lineNumbers');
        }
        else
        {
          codeMirror.setOption('lineNumbers', value);
        }
      }
    });
  }

  /**
   * 
   */
  _addModeIndicator(codeMirror)
  {
    let wrap = codeMirror.getWrapperElement();
    let dialog = wrap.appendChild(document.createElement("div"));

    // Reuse the dialog css
    dialog.className = "CodeMirror-dialog CodeMirror-dialog-bottom";

    codeMirror.on('vim-mode-change', (ev) =>
    {
      switch (ev.mode)
      {
        case 'normal':
          dialog.innerText = '';
          break;
        case 'replace':
          dialog.innerText = '-- REPLACE --';
          break;
        case 'insert':
          dialog.innerText = '-- INSERT --';
          break;
        case 'visual':
          dialog.innerText = ((ev.subMode === 'linewise') ? '-- VISUAL LINE --' : ' -- VISUAL --');
          break;
      }
    });

    return dialog;
  }

  /**
   * Creates the codemirror textarea and command area.
   * @param {Object} textArea The existing textarea to be transformed.
   */
  async _makeCodeMirror(textArea)
  {
    let options = {
      mode: "text/plain",
      keyMap: "vim",
      lineWrapping: true,
      matchBrackets: true,
      showCursorWhenSelecting: true,
      inputStyle: "contenteditable",
      spellcheck: textArea.spellcheck
    };

    let optionsOverride = await this._get('code-mirror-override');

    if (optionsOverride)
    {
      try
      {
        optionsOverride = JSON.parse(optionsOverride);
        Object.assign(options, optionsOverride);
      }
      catch (ex)
      {
        console.error('your code mirror option overrides are broken');
        console.error(ex);
        return;
      }
    }

    let width = textArea.clientWidth;
    let height = textArea.clientHeight;

    // Limit the height to the window's height so that we get a scrollbar.
    height = Math.min(height, window.visualViewport.height);

    let codeMirror = CodeMirror.fromTextArea(textArea, options);

    codeMirror.setSize(width, height);

    // let modeIndicator = this._addModeIndicator(codeMirror);

    // Don't let the mode indicator cover up the active line. Unless the user
    // want to.
    // if ((typeof(optionsOverride) === 'object') && !('cursorScrollMargin' in optionsOverride))
    // {
    //   // Set some text to get the height of the mode indicator
    //   modeIndicator.innerText = '-- NORMAL --';
    //   codeMirror.setOption('cursorScrollMargin', modeIndicator.clientHeight + 4);
    //   modeIndicator.innerText = ''
    // }

    // Move the dialog to the top
    // let originalOpenDialog = codeMirror.openDialog;
    // codeMirror.openDialog = function ()
    // {
    //   arguments[2].bottom = false;
    //   return originalOpenDialog.apply(this, arguments);
    // };

    let vimrc = await this._get('.vimrc');

    if (vimrc)
    {
      for (let line of vimrc.split('\n'))
      {
        CodeMirror.Vim.handleEx(codeMirror, line);
      }
    }

    codeMirror.focus();
  }

  /**
   * 
   */
  constructor()
  {
    this._setupCodeMirror();

    document.addEventListener('keydown', (event) =>
    {
      if (event.key === 'Insert')
      {
        if (event.target.tagName === 'TEXTAREA')
        {
          event.preventDefault();
          event.stopPropagation();

          this._makeCodeMirror(event.target);
        }
      }
    });
  }
}

new Main();