#!/bin/bash

set -e

PACKAGE_CHROME=false
PACKAGE_FIREFOX=false

while test $# -gt 0; do
  case "$1" in
    --chrome) PACKAGE_CHROME=true
      ;;
    --firefox) PACKAGE_FIREFOX=true
      ;;
  esac
  shift
done

# https://stackoverflow.com/a/4774063
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

BIN_DIR="${SCRIPTPATH}/bin/"
PACKAGE_DIR="${SCRIPTPATH}/package/"

# ./build.sh

if [ -d "${PACKAGE_DIR}" ]; then
  rm -rf "${PACKAGE_DIR}"
fi

mkdir "${PACKAGE_DIR}"

if [ "$PACKAGE_CHROME" = true ]; then
  # You need crx. npm install crx
  crx pack "${BIN_DIR}" -o "${PACKAGE_DIR}/chrome.crx" -p "./key.pem"
fi

if [ "$PACKAGE_FIREFOX" = true ]; then
  pushd "${BIN_DIR}"
  zip -r -FS "${PACKAGE_DIR}/firefox.zip" "./"
  popd
fi