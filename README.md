# wasacodemirror (CodeMirror editor with vim keybindings for any web page)

Name based, but otherwise unrelated to, akahuku's [wasavi](https://github.com/akahuku/wasavi).

wasacodemirror let's you convert any `<textarea>` element into a
[CodeMirror text editor with Vim keybindings](https://codemirror.net/demo/vim.html).

## Usage

Focus a textarea, press Insert.

If the textarea is a child of a `<form>`, CodeMirror takes care of sending the
editor's text as the value of the original textarea. In all other cases, use
`:quit`, `:q`, or `:wq` to save your changes into the original textarea.

### Configuration

Open the extension's options/preferences page.

The .vimrc gets executed every time you create a CodeMirror editor. For example:
```
colorscheme darcula
set number
```

The "CodeMirror option overrides (JSON)" is a JSON string that overrides the
options of the CodeMirror text editors. See
[the manual](https://codemirror.net/doc/manual.html#config) for a list of
options. For example
```
{
  "lineWrapping": false
}
```

Note that the extension is not active on the options page.

#### For Firefox users

`dom.visualviewport.enabled` needs to be enabled in `about:config`.

## Why

wasavi is no longer maintained and has a bug that makes it unusable (Freezes in
visual mode, causing loss of data).

## Not a complete vim

Just like CodeMirror's Vim keybindings addon, this extension does not aim to
provide a full Vim experience. The goal of this extension is to be as slim as
possible and let CodeMirror do all the work. By doing that, we hope to keep the
extension alive with minimal maintenance.

## Additions to CodeMirror's Vim

- `:quit`, `:q`, `:wq`
- `:w`
- `:set number`
- Mode indicator

# Developer notes

## Loading unpacked

Don't load this directory as an extension. Run `./build.sh` and load `./bin/`.

## Updating to the latest CodeMirror
```
pushd ./src/content/
npm install
popd
./build.sh
```

## Packaging the extension

You need to have [crx](https://github.com/oncletom/crx) installed to package the
extension.
```
npm install crx
```

Run
```
./package.sh
```