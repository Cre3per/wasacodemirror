#! /bin/bash

set -e

for resolution in 16 32 64 128 256 512; do
  inkscape -z -e "$1/${resolution}.png" -w ${resolution} -h ${resolution} "./logo.svg" &>/dev/null
done