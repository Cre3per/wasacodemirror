#!/bin/bash

set -e

# https://stackoverflow.com/a/4774063
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

BIN_DIR="${SCRIPTPATH}/bin/"
SRC_DIR="${SCRIPTPATH}/src/"

if [ -d $BIN_DIR ]; then
  rm -rf "${BIN_DIR}"
fi

mkdir "${BIN_DIR}"

CP_FILES=(
  "./manifest.json"
  "./src/options.html"
  "./src/options.js"
  "./src/content/main.js" 
  "./src/content/main.css" 
  "./src/content/node_modules/codemirror/lib/"
  "./src/content/node_modules/codemirror/keymap/vim.js"
  "./src/content/node_modules/codemirror/addon/dialog/"
  "./src/content/node_modules/codemirror/addon/search/"
  "./src/content/node_modules/codemirror/addon/edit/"
  "./src/content/node_modules/codemirror/theme/"
)

echo "copying files"
for FILE in ${CP_FILES[*]}; do
  OUT="${BIN_DIR}/${FILE}"
  mkdir -p `dirname ${OUT}`
  cp -r "${SCRIPTPATH}/${FILE}" "${OUT}"
done

# TODO: auto-gen theme list in manifest

echo "generating logo"
pushd "./logo/"
  mkdir "${BIN_DIR}/logo/"
  ./generate.sh "${BIN_DIR}/logo/"
popd

echo "built extension in ${BIN_DIR}"